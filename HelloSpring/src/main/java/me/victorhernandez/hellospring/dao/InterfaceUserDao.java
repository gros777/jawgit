package me.victorhernandez.hellospring.dao;

import java.util.List;

import me.victorhernandez.hellospring.model.Usuario;

public interface InterfaceUserDao {
	void addUser(Usuario usuario);
	public List<Usuario> getAll();
}
